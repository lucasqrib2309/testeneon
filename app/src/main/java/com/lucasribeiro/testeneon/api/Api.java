package com.lucasribeiro.testeneon.api;

import com.lucasribeiro.testeneon.models.Transfer;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface Api {

    @GET("GenerateToken")
    Observable<String> generateToken(@Query("nome") String nome, @Query("email") String email);

    @POST("SendMoney")
    @FormUrlEncoded
    Observable<Boolean> sendMoney(@Field("ClienteId") Integer clienteId, @Field("token") String token, @Field("valor") Double valor);

    @GET("GetTransfers")
    Observable<List<Transfer>> getTransfers(@Query("token") String token);
}
