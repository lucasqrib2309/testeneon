package com.lucasribeiro.testeneon;

import android.app.Application;

import com.lucasribeiro.testeneon.dagger.components.AppComponent;
import com.lucasribeiro.testeneon.dagger.components.DaggerAppComponent;
import com.lucasribeiro.testeneon.dagger.module.common.AppModule;


public class App extends Application {

    private static AppComponent appComponent;
    private static String token;

    public static void setToken(String token) {
        App.token = token;
    }

    public static String getToken() {
        return token == null ? "" : token;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        appComponent = DaggerAppComponent.builder().appModule(new AppModule(this)).build();
        appComponent.inject(this);
    }

    public static AppComponent getComponent() {
        return appComponent;
    }
}
