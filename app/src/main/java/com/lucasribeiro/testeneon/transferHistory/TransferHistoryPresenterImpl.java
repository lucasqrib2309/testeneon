package com.lucasribeiro.testeneon.transferHistory;

import android.app.Activity;
import android.util.ArrayMap;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.Transfer;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TransferHistoryPresenterImpl implements TransferHistoryContract.TransferHistoryPresenter {
    private final TransferHistoryContract.TransferHistoryView view;
    private final InteractorContract interactor;

    public TransferHistoryPresenterImpl(TransferHistoryContract.TransferHistoryView view, InteractorContract interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {
        getTransfers();
    }


    private void getTransfers() {
        interactor.getTransfers(App.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<List<Transfer>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(List<Transfer> transfers) {
                        ArrayMap<Integer, Double> transferMap = prepareTransferMap(transfers);
                        view.loadContacts(prepareContacts(transferMap), Collections.max(transferMap.values()));
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.showErrorView();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    private List<Contato> prepareContacts(ArrayMap<Integer, Double> transfersMap) {
        List<Contato> contatos = interactor.getContacts((Activity) view);
        for (Contato contato : contatos) {
            if (transfersMap.containsKey(contato.getId())) {
                contato.setValorTotal(transfersMap.get(contato.getId()));
            }
        }
        Collections.sort(contatos, new Comparator<Contato>() {
            @Override
            public int compare(Contato contato1, Contato contato2) {
                return contato2.getValorTotal().compareTo(contato1.getValorTotal());
            }
        });

        return contatos;
    }

    private ArrayMap<Integer, Double> prepareTransferMap(List<Transfer> transfers) {
        ArrayMap<Integer, Double> transfersMap = new ArrayMap<>();
        for (Transfer transfer : transfers) {
            if (transfersMap.containsKey(transfer.getClienteId())) {
                transfersMap.put(transfer.getClienteId(), transfersMap.get(transfer.getClienteId()) + transfer.getValor());
            } else {
                transfersMap.put(transfer.getClienteId(), transfer.getValor());
            }
        }
        return transfersMap;
    }
}
