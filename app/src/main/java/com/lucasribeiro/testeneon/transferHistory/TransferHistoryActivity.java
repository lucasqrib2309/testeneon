package com.lucasribeiro.testeneon.transferHistory;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.dagger.components.DaggerTransferHistoryComponent;
import com.lucasribeiro.testeneon.dagger.module.TransferHistoryModule;
import com.lucasribeiro.testeneon.models.Contato;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TransferHistoryActivity extends AppCompatActivity implements TransferHistoryContract.TransferHistoryView {

    @Inject
    TransferHistoryContract.TransferHistoryPresenter presenter;
    @BindView(R.id.toolbar)

    Toolbar toolbar;
    @BindView(R.id.toolbar_title)

    TextView toolbarTitle;
    @BindView(R.id.rvGrafico)

    RecyclerView rvGrafico;
    @BindView(R.id.rvContatos)

    RecyclerView rvContatos;
    @BindView(R.id.container_recyclers)

    LinearLayout containerRecyclers;
    @BindView(R.id.error_layout)

    LinearLayout errorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transfer_history);
        setupComponent();
        ButterKnife.bind(this);
        initializeToolbar();
        presenter.onCreate();

    }

    private void initializeToolbar() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.historico_de_envios));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp);
        }
    }

    private void setupComponent() {
        DaggerTransferHistoryComponent.builder()
                .appComponent(App.getComponent())
                .transferHistoryModule(new TransferHistoryModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void loadContacts(List<Contato> contacts, Double maxValue) {
        containerRecyclers.setVisibility(View.VISIBLE);
        errorLayout.setVisibility(View.GONE);
        createTransferList(contacts);
        createGraphicList(contacts, maxValue);

    }

    private void createGraphicList(List<Contato> contacts, Double maxValue) {
        TransferGraphicAdapter contactsAdapter = new TransferGraphicAdapter(contacts, maxValue);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rvGrafico.setLayoutManager(layoutManager);
        rvGrafico.setHasFixedSize(true);
        rvGrafico.setAdapter(contactsAdapter);
    }

    @Override
    public void showErrorView() {
        containerRecyclers.setVisibility(View.GONE);
        errorLayout.setVisibility(View.VISIBLE);
    }

    private void createTransferList(List<Contato> contacts) {
        ContactTransfersAdapter contactsAdapter = new ContactTransfersAdapter(contacts);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvContatos.setLayoutManager(layoutManager);
        rvContatos.setHasFixedSize(true);
        rvContatos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvContatos.setAdapter(contactsAdapter);
    }


    @OnClick(R.id.btn_tentar_novamente)
    public void onViewClicked() {
        presenter.onCreate();
    }
}
