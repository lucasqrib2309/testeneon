package com.lucasribeiro.testeneon.transferHistory;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.utils.ImageLoaderCallback;
import com.lucasribeiro.testeneon.utils.TextUtil;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class TransferGraphicAdapter extends RecyclerView.Adapter<TransferGraphicAdapter.VH> {

    private final List<Contato> contatos;
    private View parent;
    private final Double maxValue;


    public TransferGraphicAdapter(List<Contato> contatos, Double maxValue) {
        this.contatos = contatos;
        this.maxValue = maxValue;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transfer_graphic_item, null);
        this.parent = parent;
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        final Contato contato = contatos.get(position);


        holder.tvValor.setText(TextUtil.doubleToString(contato.getValorTotal()));
        holder.progressBar.setMax(maxValue.intValue());
        holder.progressBar.setProgress(contato.getValorTotal().intValue());
        Picasso.with(parent.getContext())
                .load(contato.getFotoUrl())
                .into(holder.cImage, new ImageLoaderCallback(holder.cImage, holder.imageContainer));

    }

    @Override
    public int getItemCount() {
        return contatos.size();
    }

    class VH extends RecyclerView.ViewHolder {

        @BindView(R.id.circular_image)
        ImageView cImage;
        @BindView(R.id.card_view_image)
        RelativeLayout imageContainer;
        @BindView(R.id.vertical_progressbar)
        ProgressBar progressBar;
        @BindView(R.id.tvValor)
        AppCompatTextView tvValor;


        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
