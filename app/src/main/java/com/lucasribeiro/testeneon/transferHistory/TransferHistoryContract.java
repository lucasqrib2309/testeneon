package com.lucasribeiro.testeneon.transferHistory;

import com.lucasribeiro.testeneon.models.Contato;

import java.util.List;

public class TransferHistoryContract {
    public interface TransferHistoryPresenter {

        void onCreate();
    }

    public interface TransferHistoryView {


        void loadContacts(List<Contato> contacts, Double maxValue);

        void showErrorView();
    }
}
