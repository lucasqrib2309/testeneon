package com.lucasribeiro.testeneon.sendMoney;

import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.utils.ImageLoaderCallback;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.VH> {

    private final List<Contato> contatos;
    private View parent;
    private final SendMoneyContract.SendMoneyPresenter presenter;

    public ContactsAdapter(List<Contato> contatos, SendMoneyContract.SendMoneyPresenter presenter) {
        this.contatos = contatos;
        this.presenter = presenter;
    }

    @Override
    public VH onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, null);
        this.parent = parent;
        return new VH(view);
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        final Contato contato = contatos.get(position);

        holder.tvNome.setText(contato.getNome());
        holder.tvTelefone.setText(contato.getTelefone());
        holder.tvEmail.setText(contato.getEmail());
        Picasso.with(parent.getContext())
                .load(contato.getFotoUrl())
                .into(holder.cImage, new ImageLoaderCallback(holder.cImage, holder.imageContainer));

        holder.rootContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onContactSelected(contato);
            }
        });
    }

    @Override
    public int getItemCount() {
        return contatos.size();
    }

    class VH extends RecyclerView.ViewHolder {

        @BindView(R.id.tvNome)
        AppCompatTextView tvNome;
        @BindView(R.id.tvTelefone)
        AppCompatTextView tvTelefone;
        @BindView(R.id.tvEmail)
        AppCompatTextView tvEmail;
        @BindView(R.id.circular_image)
        ImageView cImage;
        @BindView(R.id.card_view_image)
        RelativeLayout imageContainer;
        @BindView(R.id.root_container)
        View rootContainer;

        public VH(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
