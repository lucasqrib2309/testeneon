package com.lucasribeiro.testeneon.sendMoney;

import android.app.Activity;

import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.Transfer;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyContract.SendMoneyView;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class SendMoneyPresenterImpl implements SendMoneyContract.SendMoneyPresenter {

    private final SendMoneyView view;
    private final InteractorContract interactor;

    @Inject
    public SendMoneyPresenterImpl(SendMoneyView view, InteractorContract interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate(SendMoneyContract.SendMoneyView view) {
        view.loadContacts(interactor.getContacts((Activity) view));
    }

    @Override
    public void onContactSelected(Contato contato) {
        view.showSendMoneyDialog(contato);
    }

    @Override
    public void sendMoney(Transfer transfer) {
        interactor.sendMoney(transfer.getClienteId(), transfer.getToken(), transfer.getValor())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(Boolean value) {
                        view.onSendMoneyResponse(value);
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.onSendMoneyResponse(false);

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
