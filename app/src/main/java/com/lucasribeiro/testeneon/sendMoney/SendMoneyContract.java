package com.lucasribeiro.testeneon.sendMoney;

import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.Transfer;

import java.util.List;


public class SendMoneyContract {
    public interface SendMoneyPresenter {
        void onCreate(SendMoneyView view);

        void onContactSelected(Contato contato);

        void sendMoney(Transfer transfer);
    }

    public interface SendMoneyView {
        void loadContacts(List<Contato> contacts);

        void showSendMoneyDialog(Contato contato);

        void onSendMoneyResponse(boolean sendStatus);
    }
}
