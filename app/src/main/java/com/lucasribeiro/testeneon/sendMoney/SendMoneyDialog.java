package com.lucasribeiro.testeneon.sendMoney;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.Transfer;
import com.lucasribeiro.testeneon.utils.ImageLoaderCallback;
import com.lucasribeiro.testeneon.utils.MoneyTextWatcher;
import com.lucasribeiro.testeneon.utils.TextUtil;
import com.squareup.picasso.Picasso;

import butterknife.BindDrawable;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

class SendMoneyDialog extends Dialog implements View.OnClickListener {

    private final SendMoneyContract.SendMoneyPresenter presenter;
    private final Contato contato;
    @BindView(R.id.progressBar)

    ProgressBar progressBar;
    @BindView(R.id.fechar)

    ImageView fechar;
    @BindView(R.id.btn_send_money)

    AppCompatButton btnSendMoney;
    @BindView(R.id.circular_image)

    ImageView circularImage;
    @BindView(R.id.card_view_image)

    RelativeLayout cardViewImage;
    @BindView(R.id.tvNome)

    AppCompatTextView tvNome;
    @BindView(R.id.tvTelefone)

    AppCompatTextView tvTelefone;
    @BindView(R.id.tvEmail)

    AppCompatTextView tvEmail;
    @BindView(R.id.edt_valor)

    EditText edtValor;
    @BindDrawable(R.drawable.ic_error)
    Drawable errorDrawable;


    SendMoneyDialog(@NonNull Context context, Contato contato, SendMoneyContract.SendMoneyPresenter presenter) {
        super(context);
        this.presenter = presenter;
        this.contato = contato;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.send_money_dialog);
        if (getWindow() != null) {
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        }
        ButterKnife.bind(this);

        Picasso.with(this.getContext())
                .load(contato.getFotoUrl())
                .into(circularImage, new ImageLoaderCallback(circularImage, cardViewImage));
        tvNome.setText(contato.getNome());
        tvEmail.setText(contato.getEmail());
        tvTelefone.setText(contato.getTelefone());
        edtValor.addTextChangedListener(new MoneyTextWatcher(edtValor));

    }

    @OnClick(R.id.btn_send_money)
     void onSendMoneyClick() {
        Transfer transfer = new Transfer();
        transfer.setClienteId(contato.getId());
        transfer.setToken(App.getToken());
        transfer.setValor(TextUtil.stringToDouble(edtValor.getText().toString()));
        if (transfer.getValor() <= 0) {
            Snackbar snackbarFalha = Snackbar.make(findViewById(R.id.root_container),
                    R.string.valor_menor_0, Snackbar.LENGTH_LONG);
            snackbarFalha.show();
            return;
        }
        btnSendMoney.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        fechar.setVisibility(View.GONE);
        presenter.sendMoney(transfer);
    }

    @OnClick(R.id.fechar)
    void onFecharClick() {
        dismiss();
    }

    void onSendDone(Boolean success) {
        progressBar.setVisibility(View.GONE);
        btnSendMoney.setVisibility(View.VISIBLE);
        fechar.setVisibility(View.VISIBLE);

        if (success) {
            Snackbar snackbarSucesso = Snackbar.make(findViewById(R.id.root_container),
                    R.string.enviado_com_sucesso, Snackbar.LENGTH_SHORT);
            snackbarSucesso.addCallback(new Snackbar.Callback() {
                @Override
                public void onDismissed(Snackbar snackbar, int event) {
                    SendMoneyDialog.this.dismiss();
                }

                @Override
                public void onShown(Snackbar snackbar) {
                }
            });

            snackbarSucesso.show();
        } else {
            Snackbar snackbarFalha = Snackbar.make(findViewById(R.id.root_container),
                    R.string.falha_ao_enviar, Snackbar.LENGTH_LONG);
            snackbarFalha.setAction(R.string.tentar_novamente, this);
            snackbarFalha.show();
        }
    }

    @Override
    public void onClick(View view) {
        onSendMoneyClick();
    }
}
