package com.lucasribeiro.testeneon.sendMoney;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.dagger.components.DaggerSendMoneyComponent;
import com.lucasribeiro.testeneon.dagger.module.SendMoneyModule;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyContract.SendMoneyPresenter;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SendMoneyActivity extends AppCompatActivity implements SendMoneyContract.SendMoneyView {

    @BindView(R.id.toolbar)

    Toolbar toolbar;
    @BindView(R.id.toolbar_title)

    TextView toolbarTitle;
    @BindView(R.id.rv_contatos)

    RecyclerView rvContatos;
    @Inject

    SendMoneyPresenter presenter;
    private SendMoneyDialog sendMoneyDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        ButterKnife.bind(this);
        initializeToolbar();
        setupComponent();
        presenter.onCreate(this);
    }

    private void initializeToolbar() {
        setSupportActionBar(toolbar);
        toolbarTitle.setText(getString(R.string.enviar_dinheiro));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_chevron_left_white_24dp);
        }
    }

    private void setupComponent() {
        DaggerSendMoneyComponent.builder()
                .appComponent(App.getComponent())
                .sendMoneyModule(new SendMoneyModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void loadContacts(List<Contato> contacts) {
        ContactsAdapter contactsAdapter = new ContactsAdapter(contacts, presenter);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        rvContatos.setLayoutManager(layoutManager);
        rvContatos.setHasFixedSize(true);
        rvContatos.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvContatos.setAdapter(contactsAdapter);
    }

    @Override
    public void showSendMoneyDialog(Contato contato) {
        sendMoneyDialog = new SendMoneyDialog(this, contato, presenter);
        sendMoneyDialog.show();
    }

    @Override
    public void onSendMoneyResponse(boolean sendStatus) {
        if (sendMoneyDialog != null)
            sendMoneyDialog.onSendDone(sendStatus);
    }
}
