package com.lucasribeiro.testeneon.interactor;

import android.content.Context;

import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.Transfer;

import java.util.List;

import io.reactivex.Observable;


public interface InteractorContract {
    Observable<String> getToken(String nome, String email);

    List<Contato> getContacts(Context context);

    Observable<Boolean> sendMoney(Integer clienteId, String token, Double valor);

    Observable<List<Transfer>> getTransfers(String token);
}
