package com.lucasribeiro.testeneon.interactor;

import android.content.Context;

import com.google.gson.Gson;
import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.api.Api;
import com.lucasribeiro.testeneon.models.Contato;
import com.lucasribeiro.testeneon.models.ListaContatos;
import com.lucasribeiro.testeneon.models.Transfer;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class InteractorImpl implements InteractorContract {
    private final Api api;


    public InteractorImpl(Api api) {
        this.api = api;
    }

    @Override
    public Observable<String> getToken(String nome, String email) {
        return api.generateToken(nome, email);
    }

    @Override
    public List<Contato> getContacts(Context context) {
        Gson gson = new Gson();
        ListaContatos contatos = new ListaContatos();
        try {
            InputStream is = context.getResources().openRawResource(R.raw.contacts);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            contatos = gson.fromJson(json, ListaContatos.class);
        } catch (IOException er) {
            er.printStackTrace();
        }
        return contatos.getContatos() != null ? contatos.getContatos() : new ArrayList<Contato>();
    }

    @Override
    public Observable<Boolean> sendMoney(Integer clienteId, String token, Double valor) {
        return api.sendMoney(clienteId, token, valor);
    }

    @Override
    public Observable<List<Transfer>> getTransfers(String token) {
        return api.getTransfers(token);
    }
}
