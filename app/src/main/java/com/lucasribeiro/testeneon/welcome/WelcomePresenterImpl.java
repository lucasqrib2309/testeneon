package com.lucasribeiro.testeneon.welcome;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.utils.Constants;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class WelcomePresenterImpl implements WelcomeContract.WelcomePresenter {

    private final WelcomeContract.WelcomeView view;
    private final InteractorContract interactor;

    @Inject
    public WelcomePresenterImpl(WelcomeContract.WelcomeView view, InteractorContract interactor) {
        this.view = view;
        this.interactor = interactor;
    }

    @Override
    public void onCreate() {
        view.loadProfileImage(Constants.PROFILE_PHOTO_URL);
        view.setName(Constants.MEU_NOME);
        view.setEmail(Constants.MEU_EMAIL);
        getToken();
    }

    private void getToken() {
        interactor.getToken(Constants.MEU_NOME, Constants.MEU_EMAIL)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .unsubscribeOn(Schedulers.io())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String token) {
                        App.setToken(token);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
