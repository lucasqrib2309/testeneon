package com.lucasribeiro.testeneon.welcome;

public class WelcomeContract {
    public interface WelcomePresenter {
        void onCreate();
    }

    public interface WelcomeView {
        void loadProfileImage(String imageUrl);

        void setName(String name);

        void setEmail(String email);
    }

}
