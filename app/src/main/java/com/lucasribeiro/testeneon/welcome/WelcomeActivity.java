package com.lucasribeiro.testeneon.welcome;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.R;
import com.lucasribeiro.testeneon.dagger.components.DaggerWelcomeComponent;
import com.lucasribeiro.testeneon.dagger.module.WelcomeModule;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyActivity;
import com.lucasribeiro.testeneon.transferHistory.TransferHistoryActivity;
import com.lucasribeiro.testeneon.utils.ImageLoaderCallback;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WelcomeActivity extends AppCompatActivity implements WelcomeContract.WelcomeView {

    @BindView(R.id.circular_image)

    ImageView profileImageView;
    @BindView(R.id.txt_name)

    AppCompatTextView tvName;
    @BindView(R.id.txt_email)

    AppCompatTextView tvEmail;
    @BindView(R.id.card_view_image)

    RelativeLayout containerCardView;
    @Inject

    WelcomeContract.WelcomePresenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        setupComponent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onCreate();
    }

    private void setupComponent() {
        DaggerWelcomeComponent.builder()
                .appComponent(App.getComponent())
                .welcomeModule(new WelcomeModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void loadProfileImage(String imageUrl) {
        Picasso.with(this)
                .load(imageUrl)
                .into(profileImageView, new ImageLoaderCallback(profileImageView, containerCardView));
    }

    @Override
    public void setName(String name) {
        tvName.setText(name);
    }

    @Override
    public void setEmail(String email) {
        tvEmail.setText(email);
    }

    @OnClick(R.id.btn_send_money)
    public void onSendMoneyClick() {
        Intent sendMoneyIntent = new Intent(this, SendMoneyActivity.class);
        startActivity(sendMoneyIntent);
    }

    @OnClick(R.id.btn_history)
    public void onhistoryClick() {
        Intent historyIntent = new Intent(this, TransferHistoryActivity.class);
        startActivity(historyIntent);
    }
}
