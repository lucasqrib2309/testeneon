package com.lucasribeiro.testeneon.dagger.components;

import com.lucasribeiro.testeneon.dagger.module.SendMoneyModule;
import com.lucasribeiro.testeneon.dagger.scope.ActivityScope;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyActivity;

import dagger.Component;


@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = SendMoneyModule.class
)
public interface SendMoneyComponent {
    void inject(SendMoneyActivity activity);
}
