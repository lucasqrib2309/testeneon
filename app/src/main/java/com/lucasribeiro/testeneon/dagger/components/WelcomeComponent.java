package com.lucasribeiro.testeneon.dagger.components;

import com.lucasribeiro.testeneon.dagger.module.WelcomeModule;
import com.lucasribeiro.testeneon.dagger.scope.ActivityScope;
import com.lucasribeiro.testeneon.welcome.WelcomeActivity;

import dagger.Component;


@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = WelcomeModule.class
)
public interface WelcomeComponent {
    void inject(WelcomeActivity activity);
}
