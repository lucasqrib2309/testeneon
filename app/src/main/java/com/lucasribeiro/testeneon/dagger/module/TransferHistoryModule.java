package com.lucasribeiro.testeneon.dagger.module;

import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.transferHistory.TransferHistoryContract;
import com.lucasribeiro.testeneon.transferHistory.TransferHistoryPresenterImpl;

import dagger.Module;
import dagger.Provides;


@Module
public class TransferHistoryModule {
    private final TransferHistoryContract.TransferHistoryView view;

    public TransferHistoryModule(TransferHistoryContract.TransferHistoryView view) {
        this.view = view;
    }

    @Provides
    public TransferHistoryContract.TransferHistoryView provideWelcomeView() {
        return this.view;
    }

    @Provides
    TransferHistoryContract.TransferHistoryPresenter providePresenter(TransferHistoryContract.TransferHistoryView view, InteractorContract interactor) {
        return new TransferHistoryPresenterImpl(view, interactor);
    }
}
