package com.lucasribeiro.testeneon.dagger.module;

import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyContract.SendMoneyPresenter;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyContract.SendMoneyView;
import com.lucasribeiro.testeneon.sendMoney.SendMoneyPresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class SendMoneyModule {
    private final SendMoneyView view;

    public SendMoneyModule(SendMoneyView view) {
        this.view = view;
    }

    @Provides
    public SendMoneyView provideSendMoneyView() {
        return this.view;
    }

    @Provides
    SendMoneyPresenter providePresenter(SendMoneyView view, InteractorContract interactor) {
        return new SendMoneyPresenterImpl(view, interactor);
    }
}
