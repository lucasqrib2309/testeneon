package com.lucasribeiro.testeneon.dagger.components;

import com.lucasribeiro.testeneon.App;
import com.lucasribeiro.testeneon.api.Api;
import com.lucasribeiro.testeneon.dagger.module.common.ApiModule;
import com.lucasribeiro.testeneon.dagger.module.common.AppModule;
import com.lucasribeiro.testeneon.dagger.module.common.InteractorModule;
import com.lucasribeiro.testeneon.interactor.InteractorContract;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(
        modules = {
                AppModule.class,
                InteractorModule.class,
                ApiModule.class
        }
)
public interface AppComponent {
    void inject(App app);

    InteractorContract welcomeInteractor();

    Api api();
}
