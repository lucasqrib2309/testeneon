package com.lucasribeiro.testeneon.dagger.components;

import com.lucasribeiro.testeneon.dagger.module.TransferHistoryModule;
import com.lucasribeiro.testeneon.dagger.scope.ActivityScope;
import com.lucasribeiro.testeneon.transferHistory.TransferHistoryActivity;

import dagger.Component;


@ActivityScope
@Component(
        dependencies = AppComponent.class,
        modules = TransferHistoryModule.class
)
public interface TransferHistoryComponent {
    void inject(TransferHistoryActivity activity);
}
