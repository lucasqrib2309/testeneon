package com.lucasribeiro.testeneon.dagger.module;

import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.welcome.WelcomeContract;
import com.lucasribeiro.testeneon.welcome.WelcomePresenterImpl;

import dagger.Module;
import dagger.Provides;

@Module
public class WelcomeModule {
    private final WelcomeContract.WelcomeView view;

    public WelcomeModule(WelcomeContract.WelcomeView view) {
        this.view = view;
    }

    @Provides
    public WelcomeContract.WelcomeView provideWelcomeView() {
        return this.view;
    }

    @Provides
    WelcomeContract.WelcomePresenter providePresenter(WelcomeContract.WelcomeView view, InteractorContract interactor) {
        return new WelcomePresenterImpl(view, interactor);
    }

}
