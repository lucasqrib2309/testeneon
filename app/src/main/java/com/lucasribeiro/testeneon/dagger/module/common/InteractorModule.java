package com.lucasribeiro.testeneon.dagger.module.common;

import com.lucasribeiro.testeneon.api.Api;
import com.lucasribeiro.testeneon.interactor.InteractorContract;
import com.lucasribeiro.testeneon.interactor.InteractorImpl;

import dagger.Module;
import dagger.Provides;


@Module
public class InteractorModule {

    @Provides
    public InteractorContract provideInteractor(Api api) {
        return new InteractorImpl(api);
    }

}
