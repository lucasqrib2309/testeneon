package com.lucasribeiro.testeneon.dagger.module.common;

import com.lucasribeiro.testeneon.App;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {
    private final App app;

    public AppModule(App app) {
        this.app = app;
    }

    @Provides
    App provideApp() {
        return app;
    }
}
