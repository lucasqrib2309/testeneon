package com.lucasribeiro.testeneon.utils;

public class Constants {

    public static final String PROFILE_PHOTO_URL = "https://media.licdn.com/mpr/mpr/shrinknp_400_400/AAEAAQAAAAAAAAauAAAAJGNlZmU4MTVkLTZmNDctNDBhMi04YzFjLWE1MTMyNjZkZTEwNg.jpg";
    public static final String API_URL = "http://processoseletivoneon.azurewebsites.net/";
    public static final int API_TIMEOUT = 30;
    public static final String MEU_NOME = "Lucas Queiroz Ribeiro";
    public static final String MEU_EMAIL = "lucasqrib@hotmail.com";
}
