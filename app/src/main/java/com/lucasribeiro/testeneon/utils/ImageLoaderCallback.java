package com.lucasribeiro.testeneon.utils;


import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.lucasribeiro.testeneon.R;
import com.squareup.picasso.Callback;

public class ImageLoaderCallback implements Callback {
    private final ImageView imageView;
    private final View container;

    public ImageLoaderCallback(ImageView imageView, View container) {
        this.imageView = imageView;
        this.container = container;
    }

    @Override
    public void onSuccess() {
        Bitmap imageBitmap = ((BitmapDrawable) imageView.getDrawable()).getBitmap();
        RoundedBitmapDrawable imageDrawable = RoundedBitmapDrawableFactory.create(container.getContext().getResources(), imageBitmap);
        imageDrawable.setCircular(true);
        imageDrawable.setCornerRadius(Math.max(imageBitmap.getWidth(), imageBitmap.getHeight()) / 2.0f);
        imageView.setImageDrawable(imageDrawable);
        container.animate().alpha(1).setDuration(500).setInterpolator(new DecelerateInterpolator());
    }

    @Override
    public void onError() {
        imageView.setImageResource(R.drawable.ic_error);
        container.animate().alpha(1).setDuration(500).setInterpolator(new DecelerateInterpolator());
    }
}
