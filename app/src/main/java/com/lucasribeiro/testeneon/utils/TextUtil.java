package com.lucasribeiro.testeneon.utils;

import java.text.NumberFormat;

public class TextUtil {

    public static Double stringToDouble(String valor) {
        if (valor.equals("")) valor = "0,00";
        valor = valor.replaceAll("\\.", "");
        valor = valor.replaceAll(",", ".");

        return Double.parseDouble(valor);
    }

    public static String doubleToString(Double valor) {
        NumberFormat format = NumberFormat.getCurrencyInstance();
        return format.format(valor);
    }
}
