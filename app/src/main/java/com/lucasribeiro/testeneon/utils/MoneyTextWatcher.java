package com.lucasribeiro.testeneon.utils;

import android.text.Editable;
import android.text.Selection;
import android.text.TextWatcher;
import android.widget.EditText;


public class MoneyTextWatcher implements TextWatcher {
    private final EditText editText;

    public MoneyTextWatcher(EditText editText) {
        this.editText = editText;
    }


    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        if (!charSequence.toString().matches("(\\d{1,3}(\\.\\d{3})*|(\\d+))(\\,\\d{2})")) {

            String userInput = "" + charSequence.toString().replaceAll("[^\\d]", "");
            userInput = userInput.replaceAll("[^0-9]+", "");

            StringBuilder valorBuilder = new StringBuilder(userInput);

            while (valorBuilder.length() > 3 && valorBuilder.charAt(0) == '0') {
                valorBuilder.deleteCharAt(0);
            }
            while (valorBuilder.length() < 3) {
                valorBuilder.insert(0, '0');
            }
            if (valorBuilder.length() > 11) {
                valorBuilder.deleteCharAt(valorBuilder.length() - 1);
            }


            if (valorBuilder.length() > 5) {
                valorBuilder.insert(valorBuilder.length() - 5, ".");
            }
            if (valorBuilder.length() > 9) {
                valorBuilder.insert(valorBuilder.length() - 9, ".");
            }


            valorBuilder.insert(valorBuilder.length() - 2, ',');

            editText.setText(valorBuilder.toString());
            Selection.setSelection(editText.getText(), valorBuilder.toString().length());
        }
    }

    @Override
    public void afterTextChanged(Editable editable) {
    }
}
