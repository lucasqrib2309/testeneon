package com.lucasribeiro.testeneon.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;



public class ListaContatos {
    @SerializedName("contatos")
    private List<Contato> contatos;

    public List<Contato> getContatos() {
        return contatos;
    }

}
