package com.lucasribeiro.testeneon.models;

import com.google.gson.annotations.SerializedName;


public class Contato {

    @SerializedName("id")
    private Integer id;
    @SerializedName("nome")
    private String nome;
    @SerializedName("fotoUrl")
    private String fotoUrl;
    @SerializedName("email")
    private String email;
    @SerializedName("telefone")
    private String telefone;

    private Double valorTotal = 0.00;

    public Double getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(Double valorTotal) {
        this.valorTotal = valorTotal;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getFotoUrl() {
        return fotoUrl;
    }

    public Integer getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

}
