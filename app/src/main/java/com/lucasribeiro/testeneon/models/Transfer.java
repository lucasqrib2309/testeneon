package com.lucasribeiro.testeneon.models;

import com.google.gson.annotations.SerializedName;

import java.util.Date;


public class Transfer {
    @SerializedName("Id")
    private Integer id;
    @SerializedName("ClienteId")
    private Integer clienteId;
    @SerializedName("Valor")
    private Double valor;
    @SerializedName("Token")
    private String token;
    @SerializedName("Data")
    private Date data;


    public Integer getClienteId() {
        return clienteId;
    }

    public void setClienteId(Integer clienteId) {
        this.clienteId = clienteId;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

}
